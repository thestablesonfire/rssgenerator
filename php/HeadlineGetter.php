<?php

class HeadlineGetter
{
    private $fileLocation = '../feedfile.txt';
    private $headlineText = '';
    private $curSrc = '';
    private $totalLen = 0;
    private $headlineCount = 0;

    // Grabs all RSS source URLs from feedfile.txt and loads them into an array
    private function readSourceList()
    {
        $srcs = array();
        $feedFile = fopen($this->fileLocation, 'r') or die("Unable to open file!");
        while(!feof($feedFile)) {
            $line = (string) trim(fgets($feedFile));
            if($line) array_push($srcs, $line);
        }
        fclose($feedFile);
        return $srcs;
    }

    // Returns a DomDocument with the source XML loaded
    private function getXML($src)
    {
        return DOMDocument::load($src);
    }

    // Prints the <title>s of the XML items passed in
    private function printTitles($items, $titleLimit = 5)
    {
        $string = '';
        for ($i=0; $i<$titleLimit; $i++) {
            if($items->item($i)) {
                $item_title=$items->item($i)->getElementsByTagName('title')->item(0)->childNodes->item(0)->nodeValue;
                $this->headlineCount++;
                $this->totalLen += strlen($item_title);
                $string = $string . ' ' . $item_title;
            } //else echo $this->curSrc;
        }
        
        return $string;
    }

    public function getAvgHeadlineLength()
    {
        //echo (string)$this->totalLen . 'totalLen<br>';
        return ceil($this->totalLen / $this->headlineCount);
    }

    public function getHeadlineText() {
        //$this->headlineText = strtolower($this->headlineText);
        /*$sanitize = [
            '’',
            ':',
            '!',
            '\'',
            '"',
            '”',
            '“',
            '–',
            '‘',
            '|',
        ];
        $this->headlineText = str_replace($sanitize, '', $this->headlineText);
        */
        return $this->headlineText;
    }

    public function __construct()
    {
        $srcList = self::readSourceList();
        for($srcCount = 0; $srcCount < count($srcList); $srcCount++) {
            $xmlDoc = self::getXML($srcList[$srcCount]);
            if($xmlDoc) {
                $this->curSrc = $srcList[$srcCount];
                $items=$xmlDoc->getElementsByTagName('item');
                $this->headlineText = $this->headlineText . self::printTitles($items);
            } else {
                echo 'failed to retrieve data from ' . $this->curSrc . '<br>';
            }
        }
    }
}
