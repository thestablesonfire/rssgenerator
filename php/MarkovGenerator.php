<?php

    /**
    *   This is taken mostly directly from https://github.com/hay/markov
    */

    require 'HeadlineGetter.php';

    class MarkovGenerator
    {
        private $debug = true;
        private $text = '';
        private $markovTable = [];

        public function __construct($order = 7, $length = 1000)
        {
            $this->lookahead = $order;
            $this->length = $length;

            $headlineGetter = new HeadlineGetter();
            $this->text = $headlineGetter->getHeadlineText();
            //$this->length = $headlineGetter->getAvgHeadlineLength();
            $this->markovTable = self::generateMarkovTable();
            $this->text = self::generateMarkovText();
            echo 'Headline Generated with order ' . $order . '<br><br>';
            echo $this->text;
        }

        private function generateMarkovTable()
        {
            $lookahead = $this->lookahead;
            $text = $this->text;
            $table = array();

            for($i = 0; $i < strlen($text); $i++) {
                $char = substr($text, $i, $lookahead);
                if(!isset($table[$char])) $table[$char] = array();
            }

            for($i = 0; $i < (strlen($text) - $lookahead); $i++) {
                $char_index = substr($text, $i, $lookahead);
                $char_count = substr($text, $i+$lookahead, $lookahead);
                if (isset($table[$char_index][$char_count])) {
                    $table[$char_index][$char_count]++;
                } else {
                    $table[$char_index][$char_count] = 1;
                }
            }
            return $table;
        }

        private function generateMarkovText() {
            $table = $this->markovTable;
            $lookahead = $this->lookahead;
            $length = $this->length;
            
            // get first character
            $char = array_rand($table);
            $o = $char;
            for ($i = 0; $i < ($length / $lookahead); $i++) {
                $newchar = self::returnWeightedChar($table[$char]);
                if ($newchar) {
                    $char = $newchar;
                    $o .= $newchar;
                } else {
                    $char = array_rand($table);
                }
            }
            return $o;
        }

        private function returnWeightedChar($array) {
            if (!$array) return false;
            $total = array_sum($array);
            $rand  = mt_rand(1, $total);
            foreach ($array as $item => $weight) {
                if ($rand <= $weight) return $item;
                $rand -= $weight;
            }
        }
    }

    $order = $_GET['order'];
    $markov = new MarkovGenerator($order);

?>